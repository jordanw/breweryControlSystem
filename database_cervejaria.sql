CREATE DATABASE cervejaria;

USE cervejaria;

CREATE TABLE IF NOT EXISTS pesquisa(
	id int not null auto_increment,
    idade int, 
    estado_civil ENUM('Solteiro(a)', 'Casado(a)', 'Divorciado(a)', 'Viúvo)(a)','Separado(a)'),
    profissao VARCHAR(250),
    renda_mensal ENUM('Até um salário mínimo', 'De dois a três salários mínimos', 'De três a quatro salários mínimos',
						'Acima de quatro ou mais salários mínimos'),
    escolaridade ENUM('1º Grau incompleto',' 1° Grau completo', '2° Grau incompleto',' 2° Grau completo', 
					   'Superior incompleto','Superior completo'),
    realizacao_profissioanl text,
    importa_produto text,
    degustar_artesanal TEXT,
    degustar_acompanhamento ENUM('Nenhum', 'Petiscos', 'Refeições', 'Outro'),
    beber_frequecia ENUM('Diariamente', '1 vez na semana','2 a 4 vezes na semana', '1 vez a cada duas semanas', '1 vez por mês'),
    lugar_consumo ENUM('Em casa', 'Com amigos', 'Casa de parentes', 'Bares/restaurantes/cervejarias'),
    quantidade_rotulos ENUM('1', '2', '3', '4 ou mais'),
    acompanhante ENUM('Sozinho(a)', 'Família', 'Amigos', 'Namorado(a)'),
    media_preco_consome ENUM('Até R$20,00', 'De R$20,00 a R$50,00', 'De R$50,00 a R$80,00', 'De R$80,00 a R$100,00', 'Acima de R$100, 00'),
    compra_via_internet ENUM('sim, não'),
    dificuldade_adquirir_cerveja_artesanal TEXT,
    PRIMARY KEY(id)
    
)default charset utf8;

DESC pesquisa;